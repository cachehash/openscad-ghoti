use <joint.scad>
$fn=100;
module headShape(a=60, h=10, r=17, t=4) {
	difference () {
		translate([0,0,-h/2]) scale ([1.8,1,1]) cylinder(h,r,r);
		hull() {
			rib(a=a,h=2*h, R=60, t=t);
			D=8000;
			translate ([-D,0,0]) cylinder(r=D/2);
		}
	}
}
module mouth(t=3,h=20) {
	r1=40;
	r2=80;
	translate([0,0,-h/2])
	difference() {
		translate([0,t-r1,0]) cylinder(h=h,r=r1);
		translate([0,-r2,-h/2]) cylinder(h=2*h,r=r2);
	}
}
module headJointless(a=60,h=10,r=17, t=4) {
	difference() {
		headShape(a=a,h=h,r=r,t=t);
		translate([2*r,-r/4,0]) rotate([0,0,-10]) mouth(h=2*h);
		translate([r, 2*r/5, -h]) rotate([0,0,-20]) scale([1.5,1,1]) cylinder(h=2*h, r=r/6);
	}
}
module head(a=60,h=10,r=17,t=4, jh=5, js=2) {
	difference() {
		translate([-t,0,0]) headJointless(a=a,h=h,r=r,t=t);
		difference() {
			translate([-t,-jh/2,-h]) {
				cube([t*2+js,jh,2*h]);
			}
			roundrib(a=a,t=t,R=100);
		}
	}
}
