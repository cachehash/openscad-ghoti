spine(6);
//roundrib();
$fn=100;
module rib(a=45,h=10,t=4,R=45) {
	translate([0,0,-h/2]) {
		linear_extrude(height=h) {
			translate([-R,0,0]) {
				rotate(a/2) {
					projection() {
						linear_extrude(twist=a) {
							translate([R,0,0]) {
								circle(t/2);
							}
						}
					}
				}
			}
		}
	}
}
module spine(n, l=11, a=[36, 39, 36, 29, 24, 14, 13, 11, 9, 7, 6],R=45) {

	for (x=[1:n]) {
		alpha=a[n-x]+n;
		translate ([l*x,0,0]) {
			vertibrae(a=alpha,R=R,l=l);
		}
	}
}
/*
   a = alpha
   h = height
   t = thickness
   R = radius of curvature
   l = distance to next joint
   jh = joint height
   js = joint spacing
 */
module ccube(w,h,d) {
	translate([-w/2,-h/2,-d/2]) {
		cube([w,h,d]);
	}
}
module vertibrae(a=45,h=10,t=4,R=100,l=10,jh=5, js=2) {
	difference() {
		rib(a=a,h=h,t=t,R=R,$fn=$fn/2);
		difference() {
			translate([-t,-jh/2,-h]) {
				cube([t*2,jh,2*h]);
			}
			roundrib(a=a,t=t,R=R);
		}
	}
	translate([t/4+l/4,0,0]) {
		difference() {
			ccube(t/2+l/2, jh+js, h);
			translate([-1,0,0]) {
				ccube(t/2+l/2+2, jh, 2*h);
			}
		}
	}
	translate([t/2+2*js, -jh/2, -h/2]) {
		cube([l/2-2*js, jh, h]);
	}
	translate([l,0,0]) {
		difference() {
			ccube(l/2+2*js, jh-js, h);
			roundrib(a=180,t=t+js,R=R);
		}
	}
}
module roundrib(a=45,t=4,R=45) {
	translate([-R,0,0]) {
		rotate(-a/2) {
			rotate_extrude(angle=a) {
				translate([R,0,0]) {
					circle(t/2);
				}
			}
		}
	}
}
