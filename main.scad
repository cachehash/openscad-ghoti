use <joint.scad>
use <head.scad>

$fn=100;
//mouth();
//spine(6);
//translate ([80,0,0])
module skeleton(n=6, l=11, a=[36, 39, 36, 29, 24, 14, 13, 11, 9, 7, 6],R=45) {
	spine(n=n,l=l,a=a,R=R);
	translate ([l*(n+1),0,0]) {
		head();
	}
	vertibrae(a=36, R=R, l=l);
	hull() difference() {
		vertibrae(a=36, R=R, l=l);
		H=50;
		translate([l/2,-H/2,-H]) cube([H,H,2*H]);
	}
}
skeleton();
